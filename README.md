# ade-resource-management-game-component

/**
* Agent Development Environment (ADE)
*
* @version 1.0.1
* @author Lixiao Zhu
*
* Copyright 2020 Lixiao Zhu, Tom Williams and MIRRORLab (mirrorlab.mines.edu)
* 
* All rights reserved. Do not copy and use without permission.
* For questions contact Tom Williams at twilliams@mines.edu

**/

The resource management game is implemented for a human-subject experiment to find the relationship between proactive explanations by robots and human trust. The game is integrate with Agent Development Environment (ADE), which supports Java and C++ code. This project is implemented in Java to have a better performance on communication with other components in ADE.

**Introduction**

This work is designed and implemented for a human-subject study. The goal of the study is to determine how human trust of a robot is impacted, depending on how and when the robot provides **proactive explanation** (PE) to the human participant. From my thesis proposal,

> The definition of proactive explanations is that an autonomous system spontaneously generates an explanation to explain its next actions to human users. This provides information to human users so that they can understand the actions taken by these automation systems. Proactive explanations in this study are categorized into three major types. The first type is that robots do not spontaneously provide any explanation of their actions to their human teammates. The second type of proactive explanation is to give explanations to humans with information about robots' actions. The third type of proactive explanation informs human participants both robots' actions and profound reasons behind robots' incoming actions.

 A participant will attend the experiment by playing the game three times under different types of PEs. The results collected from the experiments will be used to train and test a **Proactive Explanation -  Partially Observable Markov Decision Process** (PE-POMDP) to enable robots to optimally provide PEs during a human-robot interaction (HRI). ADE allows the game component to connect to the turtlebot component, which controls the movement of turtlebots. The turtlebot component has integrated ROS libraries. Therefore, no need to run ROS along with the ADE. 


**Usage**

0. Physically and Mentally prepared :shipit:
1. Install `ade-2-interface_controller_merge` branch (Make sure the ade can be compiled). The repo is hosted on Gitlab. 
2. `cd ade-2-interface_controller_merge` Place the resource_management_game folder under /com folder
3. Replace the existing src-build and src-run files. You can also manually update the build and run files.
4. `./ant` to compile all code or `./ant resource_management_game` to only compile the game component.
4. `./ant run-registry` + `./ant run-turtlebotcomponent` to have control on the turtlebot robot. Remember to run each component individually.
5. `./ant run-gamecomponent` to run the game.

**Experiment Timeline**

We are aiming to have minimum 45 (up to 60) human participant to attend the experiment from 02/25/20 - 03/15/20.
- [x] 02/25/20 Expected 6 participants. 3 participants attended.
- [x] 02/26/20 Expect 4 participants. 3 participants attended.
- [x] 02/27/20 Expect 1 participant. 1 participant attended.
- [x] 02/28/20 Expect 8 participants. 3 participant attended.
- [x] 03/02/20 Expect 2 participants. 2 participant attended.
- [x] 03/03/20 Expect 3 participants. 3 participant attended.
- [x] 03/04/20 Expect 3 participants. 3 participant attended.
